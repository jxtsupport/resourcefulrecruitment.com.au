!(function($){
	$(function(){

		var searchButtonSelector = "#btn-widget-search, #rbtn-widget-search-link";		
		$("form").first().keypress(function(e){
			if ( 13 == e.which )
			{
				$(searchButtonSelector).mousedown();
				$(searchButtonSelector).click();
				return false;
			}
		});
		// custom select
		if ( $.fn.customSelect )
		{
			$(".resourcefulrecruitment_search-field select").customSelect();
		}

	});
})(jQuery);