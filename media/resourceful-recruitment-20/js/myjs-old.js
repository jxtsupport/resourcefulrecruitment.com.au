!(function ($) {
	// regular js
	function formatDate(myDate) {
		var monthList = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
		var myDay = "<span class='rss-item-pubDate-date'>" + myDate.getUTCDate() + "</span> ";
		var myMonth = "<span class='rss-item-pubDate-month'>" + monthList[myDate.getUTCMonth()] + "</span> ";
		var myYear = "<span class='rss-item-pubDate-full-year'>" + myDate.getUTCFullYear() + "</span> ";

		return myDay + "<br>" + myMonth;
	}

	// jquery
	$(function () {


		$('link[href="/media/COMMON/newdash/lib/bootstrap.min.css"]').remove();

		if ($('#site-topnav .user-loggedIn').length) {
			$('a#HiddenMemLog').prop("href", "/member/default.aspx").text('My Dashboard');
		}

		var currentPage = window.location.pathname.toLowerCase();

		
		// remove empty li's on the system pages. 
		$("#side-left li:empty").remove();

		// remove empty left side bar
		if ($('#prefix_left-navigation').children().length == 0) {
			$('#prefix_left-navigation').remove();
		}
		if ($('#side-left').children().length == 0) {
			$('#side-left').remove();
		}
		if ($('#job-side-column').children().length == 0) {
			$('#job-side-column').remove();
		}


		/* Adding Bootstrap Classes */
		// Section > Div.container
		$('#dynamic-container, #content-container, #job-dynamic-container').addClass('container');

		// Left side bar column
		$('#dynamic-side-left-container, #job-side-column, #side-left').addClass('hidden-xs col-sm-4 col-md-3');



		// dynamic side columns column
		$('#dynamic-side-right-container, #side-right').addClass('hidden-xs hidden-sm hidden-md hidden-lg');
		if (!$("#r_full-width").length) {
			$('#dynamic-side-left-container, #side-left, #job-side-column').addClass('hidden-xs col-sm-4 col-md-3');
			if ($.trim($('#dynamic-side-left-container, #side-left, #job-side-column').html()).length) {
				$('#dynamic-content, #content').addClass('col-xs-12 col-sm-8 col-md-9');
			} else {
				$('#dynamic-content, #content').addClass('col-sm-12 col-md-12');
			}
		} else {
			$('#dynamic-content, #content').addClass('col-sm-12 col-md-12');
			$('#dynamic-side-left-container, #side-left, #job-side-column').addClass('hidden-xs hidden-sm hidden-md hidden-lg');
		}
// move inner page title and text
	$(".inner-banner-text").prepend($("#dynamic-content p.heading-text, #content-container #content p.heading-text"));
	$(".inner-banner-text").prepend($("#dynamic-content h1:first, #content-container #content h1:first"));

	//Append inner banner image
		if ($('.inner-banner-image').length) {
			var manual_path = $('.inner-banner-image').attr('style');
			$('.inner-banner-bg').attr('style', manual_path);
		}

	if($('.full-width').length > 0){
		$('body').addClass('full-width-container');
	}
	if($('.team-detail-sec').length > 0){
		$('body').addClass('team-detail-page');
	}
	if($('.job-holder').length > 0){
		$('body').addClass('job-list-page');
	}
	if($('.memberprofilelinks-wrap').length > 0){
		$('body').addClass('dashboard-page');
	}
		// Content column
		// if (!$.trim($('#dynamic-side-right-container').html()).length || !$.trim($('#side-right').html()).length || $.trim($('#job-side-column').html()).length) {
		// 	if (!$("#r_full-width").length) {
		// 		$('#dynamic-content, #content-container #content').addClass('col-sm-8 col-md-9');
		// 	} else {
		// 		$('#dynamic-content, #content').addClass('col-sm-12 col-md-12');
		// 	}
		// } else {
		// 	$('#dynamic-content, #content').addClass('col-sm-8 col-md-6');
		// }
		/* $('#job-dynamic-container #content').addClass('col-sm-8 col-md-9'); */

		// form elements style
		$('input:not([type=checkbox]):not([type=radio]):not([type=submit]):not([type=reset]):not([type=file]):not([type=image]):not([type=date]), select, textarea').addClass('form-control');
		$('input[type=text]').addClass('form-control');
		$('input[type=submit]').addClass('btn btn-primary');
		$('.mini-new-buttons').addClass('btn btn-primary');
		$('input[type=reset]').addClass('btn btn-default');

		// Repsonsive image
		$('.dynamic-content-holder img').addClass('img-responsive');

		// Responsive table
		$('.dynamic-content-holder table, .content-holder table').addClass('table table-bordered').wrap('<div class="table-responsive"></div>');

		// Convert top menu to Boostrap Responsive menu
		$('.navbar .navbar-collapse > ul').addClass('nav navbar-nav');
		$('.navbar .navbar-collapse > ul > li').has('ul').addClass('dropdown');
		$('.navbar .navbar-collapse > ul > li.dropdown > a').addClass('disabled');
		$('.navbar .navbar-collapse > ul > li.dropdown').append('<a id="child-menu"></a>');
		$('.navbar .navbar-collapse > ul > li.dropdown > a#child-menu').append('<b class="caret"></b>').attr('data-toggle', 'dropdown').addClass('dropdown-toggle');
		$('.navbar .navbar-collapse > ul > li > ul').addClass('dropdown-menu');

		$('#tbKeywords,#keywords').attr('placeholder','Keywords');

		// add placeholder for search widget text field
		$('#keywords1').attr('placeholder', 'Keywords search');

		// add active class to links.
		$("li a[href='" + window.location.pathname.toLowerCase() + "']").parent().addClass("active");

		$("li li.active").parent().parent().addClass("active");
		$("li.active li.active").parent().closest("li.active").removeClass("active");

		// add last-child class to navigation 
		$("#prefix_navigation > ul > li:last").addClass("last-child");

		// add btn style
		$(".backtoresults a").addClass("btn btn-default");
		$(".apply-now-link a").addClass("btn btn-primary");
		$(".job-navbtns a").addClass("btn btn-default");

		//.left-hidden show
		if ((document.URL.indexOf("/advancedsearch.aspx") >= 0)) {
			$(".left-hidden").css("display", "block");
		}
		if ((document.URL.indexOf("/advancedsearch.aspx?") >= 0)) {
			$(".left-hidden").css("display", "none");
		}
		if ((document.URL.indexOf("/member/createjobalert.aspx") >= 0)) {
			$(".left-hidden").css("display", "block");
		}
		if ((document.URL.indexOf("/member/login.aspx") >= 0)) {
			$(".left-hidden").css("display", "block");
		}
		if ((document.URL.indexOf("/member/register.aspx") >= 0)) {
			$(".left-hidden").css("display", "block");
		}

		// Contact - Google map
		$("#footer").prepend($("#contact-map"));


		// generate select navigation from sidebar Dynamic menu
		$("#dynamic-content").convertNavigation({
			title: "Related Pages",
			links: "#site-topnav .navbar-nav li.active a:not([data-toggle=dropdown])"
		});

		// generate actions button on Job Listing page
		$(".job-navbtns").convertButtons({
			buttonTitle: "Actions&hellip;",
			title: "Please choose&hellip;",
			links: ".job-navbtns a"
		});

		// generate filters button on Job Listing page
		$(".job-navbtns").convertFilters({
			buttonTitle: "Filters&hellip;",
			filteredTitle: "Applied Filters",
			title: "Please choose&hellip;",
			filtered: ".search-query p",
			list: "ul#side-drop-menu",
			excludeFromList: "#AdvancedSearchFilter_PnlCompany"
		});

		/* System Page Forms */
		if (currentPage == "/member/createjobalert.aspx") {
			setTimeout('__doPostBack(\'ctl00$ContentPlaceHolder1$ucJobAlert1$ddlProfession\',\'\')', 0);
			Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
				$('.alternate > li > select, #ctl00_ContentPlaceHolder1_ucJobAlert1_txtSalaryLowerBand, #ctl00_ContentPlaceHolder1_ucJobAlert1_txtSalaryUpperBand').addClass('form-control');
				$('#ctl00_ContentPlaceHolder1_ucJobAlert1_ddlProfession, #ctl00_ContentPlaceHolder1_ucJobAlert1_ddlRole, #ctl00_ContentPlaceHolder1_ucJobAlert1_ddlLocation, #ctl00_ContentPlaceHolder1_ucJobAlert1_lstBoxArea, #ctl00_ContentPlaceHolder1_ucJobAlert1_ddlSalary').addClass('form-control');
			});
		}
		$(document).ajaxComplete(function () {
			$('#divRoleID1 > select, #divAreaDropDown1 > div > select').addClass('form-control');
			$('#divRoleID > select, #divAreaDropDown > div > select').addClass('form-control');
		});
		$('#salaryID').change(function () {
			$(document).ajaxComplete(function () {
				$('#divSalaryFrom > input').addClass('form-control');
				$('#divSalaryTo > input').addClass('form-control');
			});
		});

		function SalaryFromChange1() {
			$(document).ajaxComplete(function () {
				$('#divSalaryTo1 > input').addClass('form-control');
				$('#divSalaryFrom1 > input').addClass('form-control');
			});
		}

		if (currentPage == "/member/register.aspx") {
			$(".uniForm").addClass("border-container");
		}
		if (currentPage == "/member/createjobalert.aspx") {
			$(".uniForm").addClass("border-container");
		}
		/* Microsite Page */
		if (currentPage == "/microsite" || currentPage == "/microsite/") {
			$("body").addClass("microsite-page");
			$('#pagepiling').pagepiling({
				anchors: ['section1', 'section2', 'section3', 'section4', 'section5', 'section6'],
				menu: null,
				direction: 'horizontal',
				verticalCentered: true,
				sectionsColor: [],
				anchors: [],
				scrollingSpeed: 700,
				easing: 'swing',
				loopBottom: false,
				loopTop: false,
				css3: true,
				navigation: {
					'textColor': '#000',
					'bulletsColor': '#000',
					'position': 'right',
					'tooltips': ['section1', 'section2', 'section3', 'section4']
				},
				normalScrollElements: null,
				normalScrollElementTouchThreshold: 5,
				touchSensitivity: 5,
				keyboardScrolling: true,
				sectionSelector: '.section',
				animateAnchor: false,

				//events
				onLeave: function (index, nextIndex, direction) {},
				afterLoad: function (anchorLink, index) {},
				afterRender: function () {},
			});
		}
		/* End Microsite Page */

		/* Job Detail page */
		if ($('#job-dynamic-container').length) {
			$('body').addClass('job-detail-page');
			/* apply with socials */
			$('#ibSignInWithLinkedIn').parent().wrapAll('<div class="linkedIn-wrapper"/>');
			$('.linkedIn-wrapper, .indeed-apply-wrapper, .seek-apply-wrapper, #ctl00_ContentPlaceHolder1_ucJobApply1_divAutoLogin').wrapAll('<div id="social-apply-wrapper"/>');
			$('#social-apply-wrapper').insertAfter('#ctl00_ContentPlaceHolder1_ucJobApply1_divApplyNow')
		}
	});
	
	// Resize action
	/*$(window).on('resize', function() {

		var wi = $(this).width();

		// Mobile & Tablet
		if ( wi <= 992 ) {
			//$('#dynamic-side-left-container').before($('#dynamic-content'));
			//$('#side-left').before($('#content'));    		
			$('.navbar .navbar-collapse > ul > li.dropdown > a').removeAttr('class');
		}
		//  Desktop
		else {
			//$('#dynamic-side-left-container').after($('#dynamic-content'));
			//$('#side-left').after($('#content'));
			$('.navbar .navbar-collapse > ul > li.dropdown > a').addClass('disabled');
		} 

	});*/


	$(document).ready(function () {

		// contact page stop scrolling until clicked.
		$(".r27_map-overlay").click(function(){
			$(this).hide();
		});

		$(".tab-left-sec p").click(function(event) {
		        event.preventDefault();
		        var ind = $(this).index();
		        $(".tab-right .form-row").not(':eq(' + ind + ')').stop().slideUp();        
		        $(".tab-right .form-row").eq(ind).stop().slideToggle();
		        $(".tab-left-sec p").removeClass('active');
		        $(this).addClass('active');
		});
		if($('form[action="./login.aspx"]').length){
			var memLogLink = $(".memberlogin-links").html();
			$(memLogLink).appendTo("#member-submit-container");
		}

		if($('.job-detail-page').length){
			$('.job-detailtop-title').insertAfter('#job-ad-content h2');
		}

		//////for remove time from job template
		var datepost = $(".dateposted").text().trim().split(" ")[0];
		$(".dateposted").text(datepost);

		/////for add placeholder news page left search bar and sitesearch page placeholder
		$('#ctl00_ContentPlaceHolder1_tbKeywords,#keywords').attr('placeholder','Keywords');
		$('#ctl00_ContentPlaceHolder1_txtSearchKeyword').attr('placeholder','Search Keywords');

		//////for add placeholder to forget password page
		$('#ctl00_ContentPlaceHolder1_txtUsername').attr('placeholder','Username');
		$('#ctl00_ContentPlaceHolder1_txtEmail').attr('placeholder','Email address');

		/////for add placeholder job emailfriend page
		$('#ctl00_ContentPlaceHolder1_tbYourName').attr('placeholder','Your Name');
		$('#ctl00_ContentPlaceHolder1_tbYourEmail').attr('placeholder','Your Email');

		$(".horizontal-job-slider #joblist").each(function () {
            var dataURL = $(this).attr("data-url");
            $(this).includeFeed({
                baseSettings: {
                    rssURL: [dataURL || "/job/rss.aspx?addlocation=1"],
                    addNBSP: false
                },
                templates: {
                    itemTemplate: '<div class="o-featured-jobs__item"><div class="o-featured-jobs__hd-wrap"><h3 class="o-featured-jobs__hdg"><a href="{{link}}">{{title}}</a></h3></div><div class="o-featured-jobs__location"><span class="rssLocation"></span></div><time class="o-featured-jobs__date-added" datetime="">{{pubDate}}</time><div class="o-featured-jobs__descr">{{description}}</div></div>'
                },
                elements: {
					pubDate: formatDate,
					title: 1,
					description: 1
				},
                complete: function () {
                    // put location in a span of its own after pubdate.
                    $(this).children().each(function () {
                        var item = $(this);
                        item.find('.rssLocation').append(item.find('.xmlLocation').html());
                    });

                    $(this).slick({
                        dots: false,
			            arrows: true,
			            autoplay: false,
			            slidesToShow: 3,
			            slidesToScroll: 1,
			            infinite: true,
			            vertical: true,
			            verticalSwiping: true,
			            adaptiveHeight: true
                    });

                }


            });
        });

$(".vertical-job-slider #joblist").each(function () {
            var dataURL = $(this).attr("data-url");
            $(this).includeFeed({
                baseSettings: {
                    rssURL: [dataURL || "/job/rss.aspx?addlocation=1"],
                    addNBSP: false
                },
                templates: {
                    itemTemplate: '<div class="o-featured-jobs__item"><div class="o-featured-jobs__hd-wrap"><h3 class="o-featured-jobs__hdg"><a href="{{link}}">{{title}}</a></h3></div><div class="o-featured-jobs__location"><span class="rssLocation"></span></div><time class="o-featured-jobs__date-added" datetime="">{{pubDate}}</time><div class="o-featured-jobs__descr">{{description}}</div></div>'
                },
                elements: {
					pubDate: formatDate,
					title: 1,
					description: 1
				},
                complete: function () {
                    // put location in a span of its own after pubdate.
                    $(this).children().each(function () {
                        var item = $(this);
                        item.find('.rssLocation').append(item.find('.xmlLocation').html());
                    });

                    $(this).slick({
                        dots: false,
			            arrows: true,
			            autoplay: false,
			            slidesToShow: 2,
			            slidesToScroll: 1,
			            infinite: true,
			            adaptiveHeight: true,
			            responsive: [
			                {
			                    breakpoint: 991,
			                    settings: {
			                        slidesToShow: 1,
			                        slidesToScroll: 1,
			                    }
			                }
			            ]
			            
                    });

                }


            });
        });

// Testimonial slider
$(".team-testimonial-slider").each(function () {
            var dataURL = $(this).attr("data-url");
            $(this).includeFeed({
                baseSettings: {
                    rssURL: [dataURL || "/newsrss.aspx?category=testimonial"],
                    limit: 100,
                },
                templates: {
                    itemTemplate: '<div class="team-testimonial-item"><div class="o-clienttestimonials-slider__blkquote team-testimonial-col" ><p>{{description}}</p><h3 aria-label="Author" class="auther">{{author}}</h3></div></div>'
                },
                complete: function () {
                    if ($(this).children().length > 0) {
                        $(this).slick({
                           dots: true,
				          infinite: false,
				          speed: 300,
				          slidesToShow: 1,
				          slidesToScroll: 1,
				          arrows:false,
				          autoplay: true
                        });
                    }

                    $(".testimonial-content p").each(function (i) {
                        len = $(this).text().length;
                        if (len > 10) {
                            $(this).text($(this).text().substr(0, 250) + '...');
                        }
                    });

                }

            });
        });
 // news slider
 $(".latest-news-wrap").each(function () {
            var dataURL = $(this).attr("data-url");
            $(this).includeFeed({
                baseSettings: {
                    rssURL: [dataURL || "/newsrss.aspx?category=news"],
                    limit: 100,
                },
                templates: {
                    itemTemplate: '<article class="o-articleListItem article-home"><div class="row"><div class="col-md-5 col-sm-5 col-xs-12"><div class="o-articleListItem__col img-col-article" style="background-image: url({{imageurl}})"></div></div><div class="col-md-7 col-sm-7 col-xs-12"><div class="o-articleListItem__col o-articleListItem__col--content art-desc"><h3 class="o-articleListItem__title"><a href="{{link}}" tabindex="0">{{title}}</a></h3><div class="o-articleListItem__descr">{{description}}</div><div class="o-articleListItem__bottom"><ul class="o-articleListItem__bottomArticleInfo list-unstyled"><li class="o-articleListItem__bottomArticleInfoItem"><time class="o-item-info__date">{{pubDate}}</time><a href="{{link}}">Read more</a></li></ul></div></div></div></div></article>'
                },
                complete: function () {
                    if ($(this).children().length > 0) {
                        $(this).slick({
                          dots: false,
				            arrows: true,
				            autoplay: true,
				            slidesToShow: 3,
				            slidesToScroll: 1,
				            infinite: true,
				            adaptiveHeight: true,
				            vertical: true,
				            verticalSwiping: true
                        });
                    }

                    $(".testimonial-content p").each(function (i) {
                        len = $(this).text().length;
                        if (len > 10) {
                            $(this).text($(this).text().substr(0, 250) + '...');
                        }
                    });

                }

            });
        });

		$(".job-holder").each(function() {
            $(this).find('.locandsalary').prependTo($(this).find('.job-breadcrumbs'));
        });
        $("#side-drop-menu").prepend("<li id='AdvancedSearchFilter_PnlKeywords'><h3 href='#'>Keywords</h3><div class='jxt-keywords-input'><input type='text' id='sidebar-keyword-box' class='form-textbox form-control' placeholder='Search Keywords'></div></li>");
        $('.job-list-page .inner-banner-text').prepend('<h1>Job Search</h1><p class="heading-text">Whether you’re recruiting for a permanent role or a short-term solution, Resourceful Recruitment has the market intelligence and expertise to deliver.</p>');
		// $('.job-detail-page .grey-area-job').appendTo('.jobdetail-top');
		// $('.job-detail-page .jobdetail-top .grey-area-job, .job-detail-page .jobdetail-top .backtoresults, .job-detail-page .jobdetail-top .job-detailtop-title').wrapAll('<div class="container"></div>');
		
		// $('.job-detail-page .job-ad-mini, .job-detail-page .job-detail-centre').wrapAll('<div class="container"></div>')


		/*// Resize action
		var $window = $(window);
			// Function to handle changes to style classes based on window width
			function checkWidth() {
			if ($window.width() < 992) {
				$('.navbar .navbar-collapse > ul > li.dropdown > a').removeAttr('class');	
				}
		}
			// Execute on load
			checkWidth();			
			// Bind event listener
			$(window).resize(checkWidth);*/



		// Home services - carousel
		//$('.t-gallery').Gallerycarousel({ autoRotate: 4000, visible: 4, speed: 1200, easing: 'easeOutExpo', itemMinWidth: 250, itemMargin: 30 })
		$('ul.crsl-wrap').simplyScroll({
			frameRate: 60
		});

$("form :input").each(function (index, elem) {
    var eId = $(elem).attr("id");
    var label = null;
    if (eId && (label = $(elem).parents("form").find("label[for=" + eId + "]")).length == 1) {
        $(elem).attr("placeholder", $(label).clone().children().remove().end().text().replace(/  +/g, ''));
        //$(label).remove();
    }
});
//////for add current page class to body
var pageTitle = window.location.pathname.replace(/\//gi, " ").trim().split(" ");
if (pageTitle != "") {
    $("body").addClass(pageTitle[pageTitle.length - 1]);
    $("body").addClass('inner-pages');
    if (pageTitle.indexOf('/') > -1) {
        pageTitle = pageTitle.replace(/\//g, "-");
        $("body").addClass(pageTitle);
    }
}else{
    $('body').addClass('home-page');
}


		// Latest Jobs widget
		$("#myJobsList ul").includeFeed({
			baseSettings: {
				rssURL: "/job/rss.aspx?search=1&addlocation=1"
			},
			elements: {
				pubDate: formatDate,
				title: 1,
				description: 1
			},
			complete: function () {
				if ($(this).children().length) {
					$(this).children().each(function () {
						$(this).find('.rss-item-pubDate-month').text($(this).find('.rss-item-pubDate-month').text().substr(0, 3));
					});
				}

				if ($(this).children().length > 2) {
					$(this).simplyScroll({
						frameRate: 60
					});
				}
			}
		});

//consulant feed : meet the team page
		if ($('#team-member-pages').length) {
			$(".leadership-team").each(function () {
				var dataURL = $(this).attr("data-url");
				$(this).includeFeed({
					baseSettings: {
						rssURL: [dataURL || "/ConsultantsRSS.aspx"],
						limit: 200,
						addNBSP: false,
						repeatTag: "consultant"
					}, //end of base setting
					templates: {
						itemTemplate: '<article class="c-person-item"><div class="leadership-team-sec"><div class="img-area"><a href="/t/{{FriendlyURL}}" class="c-person-item__img-wrp"><img alt="{{FirstName}} {{LastName}}" title="{{FirstName}} {{LastName}}" class="c-person-item__img" src="{{ImageURL}}"></a></div><div class="c-person-item__info"><div class="team-disc-text"><h3 class="c-person-item__name"><a href="/t/{{FriendlyURL}}">{{FirstName}} {{LastName}}</a></h3><div class="c-person-item__position">{{PositionTitle}}</div></div></div></div></article>'
					}, //end of templates
					complete: function () {                        
						if ($(this).children().length) {                            
							$(this).each(function () {
								

							});
                            
                           
						}   
					} // end of complete function


				}); // end of include feed

			}); // end of team list each

		}






		// Equal Height	
		$.fn.eqHeights = function (options) {

			var defaults = {
				child: false
			};
			var options = $.extend(defaults, options);
			var el = $(this);
			if (el.length > 0 && !el.data('eqHeights')) {
				$(window).bind('resize.eqHeights', function () {
					el.eqHeights();
				});
				el.data('eqHeights', true);
			}
			if (options.child && options.child.length > 0) {
				var elmtns = $(options.child, this);
			} else {
				var elmtns = $(this).children();
			}

			var prevTop = 0;
			var max_height = 0;
			var elements = [];
			elmtns.height('auto').each(function () {

				var thisTop = this.offsetTop;
				if (prevTop > 0 && prevTop != thisTop) {
					$(elements).height(max_height);
					max_height = $(this).height();
					elements = [];
				}
				max_height = Math.max(max_height, $(this).height());
				prevTop = this.offsetTop;
				elements.push(this);
			});

			$(elements).height(max_height);
		};

		// Equal Height - Usage
		$('.service-holder').eqHeights();

		// if there is a hash, scroll down to it. Sticky header covers up top of content.
		if ($(window.location.hash).length) {
			$("html, body").animate({
				scrollTop: $(window.location.hash).offset().top - $(".navbar-wrapper").height() - 40
			}, 100);
		}
 		setTimeout((function(){
              $('.add-last-team').insertAfter('.leadership-team > article:last')
           }),3000);


	});



})(jQuery);