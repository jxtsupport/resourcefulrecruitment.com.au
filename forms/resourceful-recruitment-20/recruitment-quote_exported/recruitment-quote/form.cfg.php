<?php exit(0); ?> { 
"settings":
{
	"build_version" : 5509,
	"data_settings" : 
	{
		"save_database" : 
		{
			"database" : "",
			"is_present" : false,
			"password" : "",
			"port" : 3306,
			"server" : "",
			"tablename" : "",
			"username" : ""
		},
		"save_file" : 
		{
			"filename" : "form-results.csv",
			"is_present" : false
		},
		"save_sqlite" : 
		{
			"database" : "recruitment-quote.dat",
			"is_present" : false,
			"tablename" : "recruitment-quote"
		}
	},
	"email_settings" : 
	{
		"auto_response_message" : 
		{
			"custom" : 
			{
				"body" : "<!DOCTYPE html>\n<html dir=\"ltr\" lang=\"en\">\n<head><title>You got mail!</title></head>\n<body>\n<p><strong>Thanks for taking the time to fill out the form.<br/>Here's a copy of what you submitted:</strong></p>\n<p>\n[_form_results_]\n</p>\n</body>\n</html>",
				"is_from_red" : false,
				"is_present" : true,
				"key" : "custom-code(39)",
				"subject" : "Thank you for your submission"
			},
			"from" : "no-reply@resourcefulrecruitment.com.au",
			"is_present" : true,
			"to" : "[email24]"
		},
		"notification_message" : 
		{
			"bcc" : "",
			"cc" : "brent@resourcefulrecruitment.com.au",
			"custom" : 
			{
				"body" : "<!DOCTYPE html>\n<html dir=\"ltr\" lang=\"en\">\n<head><title>You got mail!</title></head>\n<body>\n<p>Someone filled out your form, and here's what they said:</p>\n<p>[_form_results_]</p>\n</body>\n</html>",
				"is_from_red" : false,
				"is_present" : true,
				"key" : "custom-code(17)",
				"subject" : "Fixed Free Recruitment Quote form has just been submitted | Resourceful Recruitment"
			},
			"from" : "forms@jxt.com.au",
			"is_present" : true,
			"replyto" : "",
			"to" : "enquiries@resourcfulrecruitment.com.au"
		}
	},
	"general_settings" : 
	{
		"colorboxautoenabled" : false,
		"colorboxautotime" : 3,
		"colorboxenabled" : false,
		"colorboxname" : "Default",
		"formname" : "",
		"is_appstore" : "0",
		"timezone" : "UTC"
	},
	"mailchimp" : 
	{
		"apiKey" : "",
		"lists" : []
	},
	"payment_settings" : 
	{
		"confirmpayment" : "<center>\n<style type=\"text/css\">\n#docContainer table {width:80%; margin-top: 5px; margin-bottom: 5px;}\n#docContainer td {text-align:right; min-width:25%; font-size: 12px !important; line-height: 20px;margin: 0px;border-bottom: 1px solid #e9e9e9; padding-right:5px;}\n#docContainer td:first-child {text-align:left; font-size: 13px !important; font-weight:bold; vertical-align:text-top; min-width:50%;}\n#docContainer th {font-size: 13px !important; font-weight:bold; vertical-align:text-top; text-align:right; padding-right:5px;}\n#docContainer th:first-child {text-align:left;}\n#docContainer tr:first-child {border-bottom-width:2px; border-bottom-style:solid;}\n#docContainer center {margin-bottom:15px;}\n#docContainer form input { margin:5px; }\n#docContainer #fb_confirm_inline { margin:5px; text-align:center;}\n#docContainer #fb_confirm_inline>center h2 { }\n#docContainer #fb_confirm_inline>center p { margin:5px; }\n#docContainer #fb_confirm_inline>center a { }\n#docContainer #fb_confirm_inline input { border:none; color:transparent; font-size:0px; background-color: transparent; background-repat: no-repeat; }\n#docContainer #fb_paypalwps { background: url('https://coffeecupimages.s3.amazonaws.com/paypal.gif');background-repeat:no-repeat; width:145px; height:42px; }\n#docContainer #fb_googlepay { background: url('https://coffeecupimages.s3.amazonaws.com/googlecheckout.gif'); background-repeat:no-repeat; width:168px; height:44px; }\n#docContainer #fb_authnet { background: url('https://coffeecupimages.s3.amazonaws.com/authnet.gif'); background-repeat:no-repeat; width:135px; height:38px; }\n#docContainer #fb_2checkout { background: url('https://coffeecupimages.s3.amazonaws.com/2co.png'); background-repeat:no-repeat; width:210px; height:44px; }\n#docContainer #fb_invoice { background: url('https://coffeecupimages.s3.amazonaws.com/btn_email.png'); background-repeat:no-repeat; width:102px; height:31px; }\n#docContainer #fb_invoice:hover { background: url('https://coffeecupimages.s3.amazonaws.com/btn_email_hov.png'); }\n#docContainer #fb_goback { color: inherit; }\n</style>\n[_cart_summary_]\n<h2>Almost done! </h2>\n<p>Your order will not be processed until you click the payment button below.</p>\n<a id=\"fb_goback\"href=\"?action=back\">Back to form</a></center>",
		"currencysymbol" : "$",
		"decimals" : 2,
		"fixedprice" : "000",
		"google" : 
		{
			"enabled" : false,
			"merchant_id" : "",
			"merchant_key" : ""
		},
		"invoicelabel" : "",
		"is_present" : false,
		"paymenttype" : "redirect",
		"paypal" : 
		{
			"api_password" : "",
			"api_signature" : "",
			"api_username" : "",
			"enabled" : false
		},
		"shopcurrency" : "USD",
		"usecustomsymbol" : false,
		"worldpay" : 
		{
			"enabled" : false,
			"id" : "",
			"secret" : "",
			"test_mode" : false
		}
	},
	"redirect_settings" : 
	{
		"confirmpage" : "<body>\n</body>",
		"gotopage" : "https://www.resourcefulrecruitment.com.au/thank-you",
		"inline" : "<h2>Thank you!</h2>\n<p>Your form was successfully submitted.</p>",
		"type" : "gotopage"
	},
	"uid" : "3732dd2383e09fec3fe795e1d922bf69",
	"validation_report" : "in_line"
},
"rules":{"role_type":{"fieldtype":"text","required":false},"select25":{"fieldtype":"dropdown","required":true,"values":["","Mining – Coal","Mining – Metalliferous","Oil & Gas – Upstream","Oil & Gas – Downstream","Oil & Gas – Midstream","Infrastructure","Utilities","Power Generation","Renewables","Engineering","Other"]},"select26":{"fieldtype":"dropdown","required":true,"values":["","$40,000 - $60,000","$60,000 - $90,000","$90,000 - $120,000","$120,000 - $150,000","$150,000 - $200,000","$200,000 - $250,000","$250,000+"]},"select27":{"fieldtype":"dropdown","required":true,"values":["","1 month","2 months","3 months","4 months","5 months","6 months"]},"company_name":{"fieldtype":"text","required":true},"name":{"fieldtype":"text","required":true},"email24":{"email":true,"fieldtype":"email","required":true},"additional":{"fieldtype":"textarea","maxlength":"10000"}},
"payment_rules":{"select25":{},"select26":{},"select27":{}},
"conditional_rules":{},
"application_version":"Web Form Builder (Windows), build 2.9.5509"
}